from gmpy2 import from_binary, to_binary, mpz

# GMPY2 doesn't expose the mpz_import and mpz_export functions.
# Instead, it uses two wrappers called to_binary and from_binary.
# These add two bytes to the front of the binary being imported.
#
# The value of the first byte signifies the type of data held in
# the buffer: 1 for mpz, 2 for xmpz, 3 for mpq, 4 for mpfr, and 5 for mpc.
# For mpz and xmpz, the value of the second byte signifies the buffer's sign:
# 0 if the value is 0, 1 if the value is positive, and 2 if the value is
# negative.
#
# The rest is expected to be the bits of the value with the least significant
# byte first, i.e. little-endian.


def be_to_mpz(buff: bytes) -> mpz:
    """Convert big-endian binary data into an mpz."""
    # If we had access to mpz_import then we would be able to
    # avoid doing this stuff in python and adding the extra type bytes!
    result_buf = bytearray(2 + len(buff))
    result_buf[:-2] = buff
    result_buf[-2] = 1
    result_buf[-1] = 1
    result_buf.reverse()
    return from_binary(bytes(result_buf))


def le_to_mpz(buff: bytes) -> mpz:
    """Convert little-endian binary data into an mpz."""
    result_buf = bytearray(2 + len(buff))
    result_buf[0] = 1
    result_buf[1] = 1
    result_buf[2:] = buff
    return from_binary(bytes(result_buf))


def mpz_to_be(x: mpz) -> bytes:
    """Convert an mpz into big-endian binary data."""
    return to_binary(x)[-1:1:-1]


def mpz_to_le(x: mpz) -> bytes:
    """Convert an mpz into little-endian binary data."""
    return to_binary(x)[2:]
