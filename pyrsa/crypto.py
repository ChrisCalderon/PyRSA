from pyrsa.keyinfo import KeyInfo
from pyrsa.mpz_utils import be_to_mpz, mpz_to_be
from gmpy2 import powmod, mod
from hashlib import sha256
from os import urandom

HASH = sha256
DIGEST_LENGTH = 32


def mask_generating_function(seed: bytes, length: int) -> bytes:
    iterations = length // DIGEST_LENGTH + ((length % DIGEST_LENGTH) > 0)
    result = bytearray()
    for i in range(iterations):
        result.extend(HASH(seed + i.to_bytes(4, 'big', signed=False)).digest())
    return bytes(result[:length])


def xor(a: bytes, b: bytes) -> bytes:
    assert len(a) == len(b), 'bytes must have same length!'
    return bytes(a_i ^ b_i for a_i, b_i in zip(a, b))


def encrypt(public_key: KeyInfo,
            message: bytes,
            label: bytes = b'') -> bytes:
    """Encrypts a message using RSA-OAEP."""
    modulus_length = public_key.modulus.bit_length()//8
    message_length = len(message)

    if message_length > modulus_length - 2*DIGEST_LENGTH - 2:
        raise ValueError('message too long')

    zero_pad = bytes(modulus_length - message_length - 2 * DIGEST_LENGTH - 2)
    data_block = HASH(label).digest() + zero_pad + b'\x01' + message
    seed = urandom(DIGEST_LENGTH)
    data_mask = mask_generating_function(seed, modulus_length - DIGEST_LENGTH - 1)
    masked_data = xor(data_block, data_mask)
    seed_mask = mask_generating_function(masked_data, DIGEST_LENGTH)
    masked_seed = xor(seed, seed_mask)
    encoded_message = b'\x00' + masked_seed + masked_data

    m = be_to_mpz(encoded_message)
    c = powmod(m, public_key.public_exponent, public_key.modulus)

    return mpz_to_be(c).rjust(modulus_length, b'\x00')


def decrypt(private_key: KeyInfo,
            ciphertext: bytes,
            label: bytes = b'') -> bytes:
    """Decrypts a message using RSA-OAEP."""
    modulus_length = private_key.modulus.bit_length()//8
    if len(ciphertext) != modulus_length:
        raise ValueError(f'decryption failed: expected {modulus_length} bytes, got {len(ciphertext)}')

    if modulus_length < 2*DIGEST_LENGTH - 2:
        raise ValueError('decryption failed')

    c = be_to_mpz(ciphertext)

    m1 = powmod(c, private_key.dP, private_key.p)
    m2 = powmod(c, private_key.dQ, private_key.q)
    h = mod(private_key.qInv * (m1 - m2), private_key.p)
    m = m2 + h * private_key.q

    # m = powmod(c, private_key.private_exponent, private_key.modulus)

    encoded_message = mpz_to_be(m).rjust(modulus_length, b'\x00')

    label_hash = HASH(label).digest()

    if encoded_message[0]:
        raise ValueError('decryption failed')

    masked_seed = encoded_message[1:1 + DIGEST_LENGTH]
    masked_data = encoded_message[1 + DIGEST_LENGTH:]
    seed_mask = mask_generating_function(masked_data, DIGEST_LENGTH)
    seed = xor(masked_seed, seed_mask)
    data_mask = mask_generating_function(seed, modulus_length - DIGEST_LENGTH - 1)
    data_block = xor(masked_data, data_mask)

    if not data_block.startswith(label_hash):
        raise ValueError('decryption failed')

    data_block = data_block[DIGEST_LENGTH:].lstrip(b'\x00')

    if data_block[0] != 1:
        raise ValueError('decryption failed')

    return data_block[1:]
