from typing import NamedTuple
from gmpy2 import mpz


class KeyInfo(NamedTuple):
    modulus: mpz = mpz()
    public_exponent: mpz = mpz()
    private_exponent: mpz = mpz()
    p: mpz = mpz()
    q: mpz = mpz()
    dP: mpz = mpz()
    dQ: mpz = mpz()
    qInv: mpz = mpz()
