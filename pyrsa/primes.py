from gmpy2 import invert, gcd, is_strong_prp, from_binary, random_state, mpz_random, mpz, mod
from math import sqrt, log2, ceil
from io import BufferedReader
from pyrsa.keyinfo import KeyInfo
from pyrsa.mpz_utils import le_to_mpz
from multiprocessing import Process, Queue, Event, cpu_count

# Default recommended modulus bits
DEFAULT_MODULUS_BITS = 2048
# Default recommended exponent.
DEFAULT_PUBLIC_EXPONENT = mpz(65537)
# Default acceptable error rate is 2^(-128)
DEFAULT_ER_NEG_LOG2 = 128

MPZ_ZERO = mpz()
MPZ_ONE = mpz(1)


def estimate_miller_rabin_rounds(bits: int, neg_log2_error: int) -> int:
    """Estimates the number of Miller-Rabin rounds necessary to get a sufficiently low false positive rate.

    Args:
        bits: The number of bits in the odd integer.
        neg_log2_error: The negative log2 of the required false positive rate.
            For example, if you require a false positive rate less than 1/2^128,
            then this should be 128.

    Returns:
        The number of rounds/reps/iterations of Miller-Rabin to run.
    """
    if bits >= 21:
        # Source for this estimate is the paper
        # “Average Case Error Estimates for the Strong Probable Prime Test.”
        #
        # p(k,t) is probability that odd k-bit composite passes t rounds of Miller-Rabin
        # for k >= 21 and k/9 >= t >= 3, we have:
        #   p(k,t) < k^(3/2) * 2^t * t^(-1/2) * 4^(2 - sqrt(tk))
        # we want to find t such that -log2(p(k,t)) >= -log2(error_rate)
        # for example, if the desired error rate is 1/(2^128), then we want
        #   -log2(p(k,t)) >= 128
        # log2(p(k,t)) < (3/2)*log2(k) + t - log2(t)/2 + 2*(2 - sqrt(tk))
        # -log2(p(k,t)) > (-3/2)*log2(k) - t + log2(t)/2 - 2*(2 - sqrt(tk))
        k = bits
        k_lim = k // 9
        k_rad = sqrt(k)
        k_term = -1.5 * log2(k)
        t = 2
        result = 0
        while result < neg_log2_error and t <= k_lim:
            t += 1
            result = k_term - t + 0.5*log2(t) - 2*(2 - k_rad*sqrt(t))

        if t <= k_lim and result >= neg_log2_error:
            return t

    # fallback to worst case estimate:
    return int(ceil(neg_log2_error / 2))


CHECK_ERROR = MPZ_ZERO, MPZ_ZERO


def check_pair(p: mpz, q: mpz) -> tuple[mpz, mpz]:
    """Checks if the pair of primes is far enough apart for RSA.

    Args:
        p: The first prime used to generate the public modulus.
        q: The second prime used to generate the public modulus.

    Returns:
        If p and q are too close together, then the result is 0, 0.
        Otherwise, the modulus and it's totient are returned.

    """

    n = p*q
    # This check prevents Fermat factorization attack
    k = n.bit_length()
    if abs(p - q).bit_length() <= (k/2 - 100):
        return CHECK_ERROR

    # Make sure modulus has correct bit length
    if k != (p.bit_length() + q.bit_length()):
        return CHECK_ERROR

    # return n, n - p - q + 1

    # return the Carmichael totient of n instead of the Euler totient.
    # Euler totient = (p - 1)*(q - 1) = p*q - p - q + 1
    # Carmichael totient = lcm(p - 1, q - 1) = (p - 1)*(q - 1)/gcd(p - 1, q - 1)
    p1 = p - 1
    q1 = q - 1
    denom = gcd(p1, q1)

    if denom > 2:
        return CHECK_ERROR

    return n, p1 * (q1 // denom)


class PrimeWorker(Process):
    def __init__(self, bits: int, public_exponent: mpz, err_neg_log2: int, result_q: Queue, stop_ev: Event):
        assert not bits & 7, 'bits must be divisible by 8'
        super().__init__()
        self.bits = bits
        self.public_exponent = public_exponent
        self.err_neg_log2 = err_neg_log2
        self.result_q = result_q
        self.stop_ev = stop_ev

    def run(self) -> None:
        small_primes_prod1 = mpz(307444891294245705)   # product of primes < 53
        small_primes_prod2 = mpz(3749562977351496827)  # product of primes < 101 and >= 53.
        e = self.public_exponent

        # Avoiding unnecessary calls to le_to_mpz
        # by maintaining and updating a buffer here
        p_buffer = bytearray(self.bits//8 + 2)
        p_buffer[0] = 1
        p_buffer[1] = 1
        p_buffer_view = memoryview(p_buffer)[2:]

        mr_rounds = estimate_miller_rabin_rounds(self.bits, self.err_neg_log2)
        random_file: BufferedReader = open('/dev/urandom', 'rb')
        rng_state = random_state(le_to_mpz(random_file.read(8)))

        while not self.stop_ev.is_set():

            random_file.readinto(p_buffer_view)
            p_buffer[-1] |= 128  # set top bit of int
            p_buffer[2] |= 1     # set bottom bit

            p = from_binary(bytes(p_buffer))

            if gcd(p, small_primes_prod1) != 1:
                continue

            if gcd(p, small_primes_prod2) != 1:
                continue

            passed_all_rounds = True
            for i in range(mr_rounds):
                base = mpz_random(rng_state, p - 4) + 2
                if gcd(p, base) != 1:
                    # found a factor of p
                    passed_all_rounds = False
                    break
                if not is_strong_prp(p, base):
                    passed_all_rounds = False
                    break

            # e, a.k.a the public exponent, must be coprime to the totient,
            # so it must be coprime to the factors of the totient.
            # if gcd(p - 1, e) != 1:
            #     continue
            #
            # Since e is prime, it only has two divisors,
            # 1 and itself. Thus, if gcd(p - 1, e) does not equal 1,
            # then it must equal e, meaning p - 1 = 0 mod e, or
            # p = 1 mod e. So skip p if p = 1 mod e.
            if mod(p, e) == MPZ_ONE:
                continue

            if passed_all_rounds:
                self.result_q.put(bytes(p_buffer))

        random_file.close()


def generate_key(modulus_bits: int = DEFAULT_MODULUS_BITS,
                 public_exponent: mpz = DEFAULT_PUBLIC_EXPONENT,
                 err_neg_log2: int = DEFAULT_ER_NEG_LOG2) -> KeyInfo:
    """Generates an RSA private key.

    Args:
        modulus_bits: The required number of bits for the public modulus.
        public_exponent: The public exponent of the RSA key.
        err_neg_log2: The acceptable false positive rate for primality testing.

    Returns:
        The generated private key.
    """
    assert not modulus_bits & 7, 'modulus_bits must be divisible by 8'

    prime_bits = modulus_bits // 2
    procs = []
    primes = []
    result_q = Queue()
    stop_ev = Event()
    for i in range(cpu_count()):
        procs.append(PrimeWorker(prime_bits, public_exponent, err_neg_log2, result_q, stop_ev))
        procs[-1].start()

    while True:
        q_binary = result_q.get()
        q = from_binary(q_binary)

        # check pairs of primes from primes already found
        # because finding primes is expensive...
        for p in primes:
            result = check_pair(p, q)
            if result is CHECK_ERROR:
                continue

            modulus, totient = result
            # found a good prime pair,
            # shut down the processes
            # and empty the queue
            stop_ev.set()
            while not result_q.empty():
                result_q.get()
            for proc in procs:
                proc.join()

            private_exponent = invert(public_exponent, totient)
            return KeyInfo(modulus,
                           public_exponent,
                           private_exponent,
                           p,
                           q,
                           mod(private_exponent, (p - 1)),
                           mod(private_exponent, (q - 1)),
                           invert(q, p))
        primes.append(q)
