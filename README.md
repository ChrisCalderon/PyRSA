# PyRSA
A Python3 implementation of RSA. Depends on gmpy2 and pyasn1.

## WARNING: DO NOT USE THIS FOR SERIOUS APPLICATIONS
I wrote this to teach myself how RSA works. It has bugs and is not safe to use. It is just for messing around.

## Usage
Here's how you use this:

```python
import pyrsa
key = pyrsa.generate_key(2048, 65537)
plaintext = b'henlo world!'
ciphertext = pyrsa.encrypt(key, plaintext)
if pyrsa.decrypt(key, ciphertext) == plaintext:
    print('Noice, it worked!')

pubkey_encoded = pyrsa.encode_public_key(key) # give this to your friends.
pubkey = pyrsa.decode_public_key(pubkey_encoded) # they can use this key to encrypt something to send to you
privkey_encoded = pyrsa.encode_private_key(key) # save this for later if you want to reuse this key.
with open('my_secret.key', 'wb') as f:
    f.write(privkey_encoded) # might want to chmod 600 this.
privkey = pyrsa.decode_private_key(privkey_encoded)
```

Generating a key with a 2048-bit modulus and 65537 public exponent takes about 75 ms on my MacBook Pro,
and a 4096-bit modulus with 65537 public exponent takes about 105 ms.

You can use the encoded private and public keys with the `openssl pkeyutl` command like this:

```shell
# Assuming pubkey.pem contains the encoded public key
echo "Yo bruh, what's up?" > msg.txt
openssl pkeyutl -encrypt -pubin -inkey pubkey.pem -in msg.txt -out msg.txt.encrypted -pkeyopt rsa_padding_mode:oaep -pkey_opt digest:sha256
# To decrypt, assuming privkey.pem contains the encoded private key...
openssl pkeyutl -decrypt -inkey privkey.pem -in msg.txt.encrypted -out msg.txt.decrypted -pkeyopt rsa_padding_mode:oaep -pkey_opt digest:sha256
```

A file encrypted by openssl in this manner can be decrypted with the `pyrsa.decrypt` function.
