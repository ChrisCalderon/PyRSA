import pyrsa

REPS = 1000
msg = b'henlo worl'

if __name__ == '__main__':
    for rep in range(1, REPS+1):
        print(f'\r{rep}', end='', flush=True)
        try:
            key = pyrsa.generate_key()
            cmsg = pyrsa.encrypt(key, msg)
            dmsg = pyrsa.decrypt(key, cmsg)
            assert dmsg == msg, 'Decryption failed!'
        except Exception as exc:
            print('\nTEST FAILED\n')
            raise exc
    print('\nTEST PASSED\n')
